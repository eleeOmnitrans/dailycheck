﻿using System;
using System.Data;
using System.Data.Common;
using Snowflake.Data.Client;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;


namespace DailyCheck
{
    class Program
    {
        static async Task Main(string[] args)
        {
            try
            {


                

                string cmdtxt;
                string s = "";

                //difference in view sb_shipments_header counts between DEV and Prod
                //cmdtxt = "select (select count(*) from prod_db.dw_us_bkge_dm.sb_shipments_header ) - (select count(*) from dev_db.dw_us_bkge_dm.sb_shipments_header ) sb_shipments_header_view_difference";
                cmdtxt = "select * from prod_db.dw_shipment_dm.sb_shipments_header_view_difference";

                DataTable dt = new DataTable();

                using (IDbConnection conn = new SnowflakeDbConnection())
                {
                    //conn.ConnectionString = "scheme=https;ACCOUNT=LW23586;HOST=lw23586.canada-central.azure.snowflakecomputing.com;port=443;ROLE=DW_DBA; USER=elee; PASSWORD=zM2K9RFF4uMERuag;DB=PROD_DB;SCHEMA=DW_STG";
                    conn.ConnectionString = "scheme=https;ACCOUNT=LW23586;HOST=lw23586.canada-central.azure.snowflakecomputing.com;port=443;ROLE=DW_VIZ; USER=PWRBI_VIEWER; PASSWORD=pL0FeGTSCMDjxejiH6qe;DB=PROD_DB;SCHEMA=dw_shipment_dm;warehouse=VIZ_WH";
                    conn.Open();
                    Console.WriteLine("Connection successful!");
                    using (IDbCommand cmd = conn.CreateCommand())
                    {

                        //cmd.CommandText = "select top 10 entid, * from dev_db.dw_stg.sb_entheader order by create_tmsp desc;";   // sql opertion fetching 
                        //data from an existing table

                        cmd.CommandText = cmdtxt;

                        IDataReader reader = cmd.ExecuteReader();



                        dt.Load(reader);

                        if (dt.Rows.Count == 1)
                        {
                            s += "Difference in view sb_shipments_header counts between DEV and Prod : <b>" + dt.Rows[0]["sb_shipments_header_view_difference"].ToString() + "</b><br><br>";
                        }

                        dt.Clear();

                        //difference in staging table sb_sbtrans counts between DEV and Prod since 2022/1/15
                        //cmdtxt = "select (select count(*) from prod_db.dw_stg.sb_sbtrans where TO_DATE(CREATE_TMSP) >= '2022-1-15' ) -(select count(*) from dev_db.dw_stg.sb_sbtrans where TO_DATE(CREATE_TMSP) >= '2022-1-15') sb_sbtrans_difference";
                        cmdtxt = "select * from PROD_DB.DW_SHIPMENT_DM.SB_SBTRANSDIFFERENCE";

                        cmd.CommandText = cmdtxt;

                        reader = cmd.ExecuteReader();



                        dt.Load(reader);

                        if (dt.Rows.Count == 1)
                        {
                            s += "Difference in staging table sb_sbtrans counts between DEV and Prod since 2022/1/15 : <b>" + dt.Rows[0]["sb_sbtrans_difference"].ToString() + "</b><br><br>";
                        }

                        dt.Clear();


                        //Last data load SBTrans Counts
                        //cmdtxt = "select 'DEV SBTrans' Datasource, TO_DATE(CREATE_TMSP) CREATE_TMSP,  count(*) RecCount from dev_db.dw_stg.sb_sbtrans where TO_DATE(CREATE_TMSP) = TO_DATE(select max(CREATE_TMSP) from dev_db.dw_stg.sb_sbtrans) "+
                        //        " group by TO_DATE(CREATE_TMSP) union select 'PROD SBTrans' Datasource, TO_DATE(CREATE_TMSP) CREATE_TMSP,  count(*) from prod_db.dw_stg.sb_sbtrans where TO_DATE(CREATE_TMSP) =" + 
                        //        " TO_DATE(select max(CREATE_TMSP) from prod_db.dw_stg.sb_sbtrans) group by TO_DATE(CREATE_TMSP)";
                        cmdtxt = "select * from prod_db.dw_shipment_dm.LoadSBTransCounts";

                        cmd.CommandText = cmdtxt;

                        reader = cmd.ExecuteReader();



                        dt.Load(reader);


                        s += "<table border=1><tr><td><b>Datasource</b></td><td><b>Created DT</b></td><td><b>Record Count</b></td></tr>";

                        foreach (DataRow dr in dt.Rows)
                        {
                            s += "<tr><td>" + dr["Datasource"].ToString() + "</td><td>" + dr["CREATE_TMSP"].ToString() + "</td><td>" + dr["RecCount"].ToString() + "</td></tr>";
                            
                        }

                        s += "</table>";

                        dt.Clear();
                        conn.Close();

                    }

                    Console.Write(s);

                    await SendMessageToTeam(s);

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        static async Task SendMessageToTeam(string msg)
        {
            #region Send message to team
            //powershell command posting message to team
            //curl.exe - H "Content-Type:application/json" - d "{'text':'Servers x is started.'}" https://omnitransinc.webhook.office.com/webhookb2/4b70ca75-92b0-4555-84be-42efdafcd9e5@df3f7c8d-2b9f-4b58-bf15-fffee0e2c88f/IncomingWebhook/4bab00897e0e4e89a3270d2aa7d839be/3c4edaaf-0626-4326-8356-a9c4d6fd9a84

            string ServerBackupStatusWebHook = ConfigurationManager.AppSettings["MetroBITeamWebHook"];
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), ServerBackupStatusWebHook))
                {
                    string SendMessageText = "{'text':'" + msg + "'}";
                    request.Content = new StringContent(SendMessageText);
                    request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

                    var response = await httpClient.SendAsync(request);
                }
            }

            #endregion
        }
    }
}
